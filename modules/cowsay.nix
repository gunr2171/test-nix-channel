{ lib, config, pkgs, ... }:
with lib;
let
  cfg = config.moog.packages.cowsay;
in {
  options.moog.packages.cowsay = {
    enable = mkEnableOption "the cowsay package";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = [ pkgs.neo-cowsay ];
  };
}
